<?php

return [
    'helpScoutApiKey' => '',
    'agentEmailDomain' => '',
    'notExportSpam' => true,
    'csvDelimiter' => ';' // for importing to MS Excel file set ';'
];
