<?php
namespace app\commands;

use app\models\Export;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class ExportConversationsController
 * @package HelpScoutFeatures
 */
class HelpScoutExportController extends Controller
{

    public function actionIndex()
    {
        include_once __DIR__.'/../views/help-scout-export/how_to_use.php';
    }

    /**
     * @param $pagesCountFrom
     * @param $pagesCountTo
     * @param $isResumeFromLastExportedPage
     * @return bool
     * @throws \Exception
     */
    public function actionExportCustomers($pagesCountFrom, $pagesCountTo, $isResumeFromLastExportedPage)
    {
        $model = new Export();
        $model->setScenario(Export::SCENARIO_EXPORT_CUSTOMERS);
        $data = [
            'pagesCountFrom'=>$pagesCountFrom,
            'pagesCountTo'=>$pagesCountTo,
            'isResumeFromLastExportedPage'=>$isResumeFromLastExportedPage
        ];
        if ($model->load($data, '') && $model->validate()) {
            $msg = '';
            foreach ($data as $param=>$value) {
                $msg .= $param.' = '.$value.PHP_EOL;
            }
            if ($this->confirm('Export customers with parameters:'.PHP_EOL.$msg.PHP_EOL.'Are you sure?')) {

            } else {
                return false;
            }

            $model->exportCustomers($pagesCountFrom, $pagesCountTo == 'all' ? null : $pagesCountTo, $isResumeFromLastExportedPage);
        }
        else {
            echo implode(PHP_EOL, $model->getFirstErrors());
        }
    }

    public function actionExportUsers()
    {
        $model = new Export();
        $model->exportUsers();
    }

    /**
     * @param $mailboxId
     * @param string $modifiedSince
     * @param string $status
     * @param string $tag
     * @return bool
     * @throws \HelpScout\ApiException
     */
    public function actionExportMessages($mailboxId, $modifiedSince, $status='all', $tag='')
    {
        $model = new Export();
        $model->setScenario(Export::SCENARIO_EXPORT_MESSAGES);
        if (
            $model->load(
                    [
                        'mailboxId' => $mailboxId,
                        'modifiedSince' => $modifiedSince,
                        'status' => $status,
                        'tag' => $tag
                    ], ''
                )
            && $model->validate()
        ) {
            if (
                !Console::confirm('Do you confirm export messages with parameters:'.PHP_EOL
                    .'from mailbox(es): '.$mailboxId.PHP_EOL
                    .'status: '.$status
                    .'modified since (UTC): '.$modifiedSince.PHP_EOL
                    .'status: '.$status.PHP_EOL
                    .($tag ? ', tag '.$tag : '').' ?')
                ) {
                return false;
            }

            $mailboxes = $model->getMailboxesIdsAndNames();
            $mailboxesIds = ($mailboxId == 'all' ? array_keys($mailboxes) : $mailboxId);
            foreach ($mailboxesIds as $curMailboxId) {
                $model->exportMessages($curMailboxId, $modifiedSince, $status, $tag);
            }
        }
        else {
            echo implode(PHP_EOL, $model->getFirstErrors());
        }

        return true;
    }

    public function actionListMailboxes()
    {
        $model = new Export();
        $mailboxes = $model->getMailboxesIdsAndNames();
        if ($mailboxes) {
            echo 'id'."\t".'name'.PHP_EOL;
            foreach ($mailboxes as $id => $name) {
                echo $id."\t".$name.PHP_EOL;
            }
        }
        else {
            echo 'No mailboxes'.PHP_EOL;
        }
    }

}
