<?php
namespace app\models;

use HelpScout\ApiClient;
use HelpScout\ApiException;
use HelpScout\Collection;
use HelpScout\model\Conversation;
use HelpScout\model\Customer;
use HelpScout\model\customer\Address;
use HelpScout\model\customer\ChatEntry;
use HelpScout\model\customer\EmailEntry;
use HelpScout\model\customer\PhoneEntry;
use HelpScout\model\customer\WebsiteEntry;
use HelpScout\model\Mailbox;
use HelpScout\model\User;
use yii\base\Model;
use yii\helpers\Console;

/**
 * Class Export
 * @package HelpScoutExport\models
 */
class Export extends Model
{
    /** int after this pages count rows will be written to csv file */
    const BUFFER_PAGE_LIMIT = 1;
    const SCENARIO_EXPORT_CUSTOMERS = 'exportCustomers';
    const SCENARIO_EXPORT_MESSAGES = 'exportMessages';

    /** @var  int */
    public $pagesCountFrom;
    /** @var  int */
    public $pagesCountTo;
    /** @var  int */
    public $mailboxId;
    /** @var  bool */
    public $isResumeFromLastExportedPage;
    public $modifiedSince;
    public $status;
    public $tag;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['pagesCountFrom', 'pagesCountTo'], 'required', 'on'=>self::SCENARIO_EXPORT_CUSTOMERS],
            [['pagesCountFrom'], 'integer', 'min'=>1, 'on'=>self::SCENARIO_EXPORT_CUSTOMERS],
            [['pagesCountTo'], 'match', 'pattern'=>'~^[0-9]+|all$~', 'message'=>'Pages Count To must be integer or "all"', 'on'=>self::SCENARIO_EXPORT_CUSTOMERS],
            [['mailboxId'], 'validateMailboxId', 'on'=>self::SCENARIO_EXPORT_MESSAGES],
            [['modifiedSince'], 'date', 'format' => 'php:Y-m-d\TH:i:s\Z', 'on'=>self::SCENARIO_EXPORT_MESSAGES, 'message' => 'modifiedSince date wrong format. Must be in format like: 2017-01-25T21:20:45Z'],
            [['status'], 'in', 'range'=>['all', 'active', 'pending']],
            [['tag'], 'string', 'on'=>self::SCENARIO_EXPORT_MESSAGES],
        ];
    }

    /**
     * @param $attribute
     * @return bool
     * @throws ApiException
     */
    public function validateMailboxId($attribute)
    {
        if ($this->$attribute == 'all') {
            return true;
        }

        $mailboxes = $this->getMailboxesIdsAndNames();
        if (!isset($mailboxes[$this->$attribute])) {
            $msg = '';
            foreach ($mailboxes as $id => $name) {
                $msg = $id.' ['.$name.'] '.PHP_EOL;
            }
            $this->addError($attribute, $attribute.' '.$this->$attribute.' not exists or it is not a mailbox of current account. '
                .'Must be "all" or integer value, one from: '.PHP_EOL.$msg);
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getCustomersCounts()
    {
        static $customersCounters = null;
        if ($customersCounters === null) {
            /** @var Collection $collection */
            $collection = $this->getApiClient()->getCustomers();
            $customersCounters = ['count'=>$collection->getCount(), 'pages'=>$collection->getPages()];
        }
        return $customersCounters;
    }

    /**
     * @param int $mailBoxId
     * @return array|\HelpScout\Collection|Mailbox
     */
    public function getMailboxesFromApi($mailBoxId=0)
    {
        static $mailboxes = null;
        if (is_null($mailboxes)) {
            $mailboxes = $this->getApiClient()->getMailboxes();
            if (!$mailboxes->getCount()) {
                return [];
            }
        }

        if ($mailBoxId && $mailboxes) {
            /** @var Mailbox $curMailBox */
            foreach($mailboxes->getItems() as $curMailBox) {
                if ($curMailBox->getId() == $mailBoxId) {
                    return $curMailBox;
                }
            }
        }
        return $mailboxes->getItems();
    }


    /**
     * @param $mailBoxId
     * @param string $modifiedSince
     * @param string $status
     * @param string $tag
     * @return int
     * @throws ApiException
     * @see http://developer.helpscout.net/help-desk-api/conversations/list/#request
     */
    public function exportMessages($mailBoxId, $modifiedSince, $status='all', $tag='')
    {
        $rows = [];
        $resultDirPath = $this->getResultDirPath();

        $rowsExportedCount = 0;
        $hasHeaders = false;

        $mailboxes = $this->getMailboxesIdsAndNames();
        $mailboxForFileName = str_replace(' ', '_', strtolower($mailboxes[$mailBoxId]));

        $saveDirPath = $resultDirPath.'messages_modified_since-'.str_replace(':', '-', $modifiedSince).'__status-'.$status.($tag ? '__tag-'.$tag : '');
        if (is_dir($saveDirPath)) {
            if (!Console::confirm('Directory '.$saveDirPath.' already exists. Data will be added to result file. Are you sure you want to export to existent directory?')) {
                return 0;
            }
        }

        $this->createDirWritableToAll($saveDirPath);
        $attachmentsDirPath = $saveDirPath.'/attachments';
        $this->createDirWritableToAll($attachmentsDirPath);
        $logFilePath = $saveDirPath.'/log.txt';
        $resultFileName = 'helpscout_messages_from_mailbox_'.$mailboxForFileName.'.csv';

        $params = ['modifiedSince'=>$modifiedSince];
        if ($status) {
            $params['status'] = $status;
        }
        if ($tag) {
            $params['tag'] = $tag;
        }

        $collection = $this->getApiClient()->getConversationsForMailbox($mailBoxId, $params);
        $pagesCount = $collection->getPages();
        $threadsCount = 0;
        $conversationsTotalCount = $pagesCount > 1 ? ($collection->getPages() * 50) : $collection->getCount();

        $logMsg = 'START export from mailbox '.$mailboxes[$mailBoxId].' (mailbox id: '.$mailBoxId.') with params: '
            .'modified since (UTC time format): '.$modifiedSince.PHP_EOL
            .'status: '.$status.PHP_EOL
            .($tag ? ' tag: '.$tag.PHP_EOL : '')
            .'spam: '.(\Yii::$app->params['notExportSpam'] ? 'ignore' : 'not ignore').PHP_EOL
            .'delimiter for the fields: '.\Yii::$app->params['csvDelimiter'].PHP_EOL
            .'Total conversations found: '.$conversationsTotalCount.PHP_EOL
            .'Data will be exported to directory: '.$saveDirPath;
        $this->log($logMsg, $logFilePath);

        for ($curPage=1; $curPage <= $pagesCount; $curPage++) {
            $collection = $this->getApiClient()->getConversationsForMailbox($mailBoxId, array_merge(['page'=>$curPage], $params));
            /** @var Conversation $curItem */
            foreach ($collection->getItems() as $index=>$curItem) {
                if (
                    (\Yii::$app->params['notExportSpam'] && $curItem->isSpam())
                ) {
                    continue;
                }

                $vars = $curItem->getObjectVars();
                $parsedVars = $this->parseConversationInArray($vars, 0, $attachmentsDirPath);
                $rows[] = $parsedVars;
                $rowsExportedCount++;

                if (!$hasHeaders) {
                    $firstRow = array_pop($rows);
                    if (empty($firstRow)) {
                        continue;
                    }
                    $rows[] = array_keys($firstRow);
                    $rows[] = $firstRow;
                    $hasHeaders = true;
                }

                if (!empty($vars['threads'])) {
                    foreach($vars['threads'] as $curThreadVars) {
                        $rows[] = $this->parseConversationInArray($curThreadVars, $vars['id'], $attachmentsDirPath);
                        $threadsCount++;
                    }
                }
            }

            if ($rows) {
                $this->exportCsv($rows, $saveDirPath, $resultFileName);
                $this->log($curPage. ' page: '.$rowsExportedCount.' conversation(s) and '.$threadsCount.' thread(s) was exported', $logFilePath);
                $rows=[];
            }
        }

        $this->log('FINISH export messages. Data was exported to file: '.$resultFileName, $logFilePath);

        return $rowsExportedCount;
    }


    /**
     * @return array
     * @throws ApiException
     */
    public function getMailboxesIdsAndNames()
    {
        $mailboxes = [];
        try {
            $mailboxesFromApi = $this->getMailboxesFromApi();
        }
        catch(ApiException $ex) {
            throw new ApiException($ex->getMessage(), $ex->getCode());
        }

        /** @var \HelpScout\model\Mailbox $curMailBox */
        foreach ($mailboxesFromApi as $curMailBox) {
            $mailboxes[$curMailBox->getId()] = $curMailBox->getName();
        }

        return $mailboxes;
    }

    /**
     * @param int $pagesCountFrom
     * @param null $pagesCountTo
     * @param bool|true $isResumeFromLastExportedPage
     * @return int
     * @throws ApiException
     * @throws \Exception
     */
    public function exportCustomers($pagesCountFrom=1, $pagesCountTo=null, $isResumeFromLastExportedPage=true)
    {
        $rows = [];
        $saveDirPath = $this->getResultDirPath().'customers/';
        $this->createDirWritableToAll($saveDirPath);
        $logFilePath = $saveDirPath.'log.txt';
        $lastExportedPageFilePath = $saveDirPath.'/last_exported_customers_page.txt';
        $hasHeaders = file_exists($saveDirPath.'/helpscout_customers.csv') && !empty(file_get_contents($saveDirPath.'/helpscout_customers.csv'));

        if ($isResumeFromLastExportedPage && file_exists($lastExportedPageFilePath)) {
            $pagesCountFrom = intval(file_get_contents($lastExportedPageFilePath)) + 1;
        }

        $collection = $this->getApiClient()->getCustomers();
        $totalCount = $collection->getCount();

        if ($totalCount) {
            $this->log('START export customers', $logFilePath);
        }
        else {
            Console::output('No customers found');
            return 0;
        }

        if ($pagesCountTo === null) {
            $pagesCountTo = $collection->getPages();
        }

        $pageIndex = 1;
        $rowsExportedCount = 0;

        for ($curPage=$pagesCountFrom; $curPage <= $pagesCountTo; $curPage++) {
            try {
                $collection = $this->getApiClient()->getCustomers($curPage);
            }
            catch(ApiException $ex) {
                throw new \Exception($ex->getMessage(), $ex->getCode());
            }

            /** @var Customer $item */
            foreach ($collection->getItems() as $item) {
                $rows[] = array_merge([
                        'id'=> $item->getId(),
                        'firstName'=> $item->getFirstName(),
                        'lastName'=> $item->getLastName(),
                        'gender'=> $item->getGender(),
                        'jobTitle'=> $item->getJobTitle(),
                        'age' => $item->getAge(),
                        'photoType' => $item->getPhotoType(),
                        'photoUrl' => $item->getPhotoUrl(),
                        'organization' => $item->getOrganization(),
                        'location' => $item->getLocation(),
                        'address' => $item->getAddress(),
                        'background' => $item->getBackground(),
                        'getCreatedAt' => $item->getCreatedAt(),
                        'getModifiedAt' => $item->getModifiedAt()
                    ], $this->getCustomerDetails($item->getId()));

                if (!$hasHeaders) {
                    $firstRow = array_pop($rows);
                    $rows[] = array_keys($firstRow);
                    $rows[] = $firstRow;
                    $hasHeaders = true;
                }
            }

            if ($pageIndex == self::BUFFER_PAGE_LIMIT) {
                $this->exportCsv($rows, $saveDirPath, 'helpscout_customers.csv');
                file_put_contents($lastExportedPageFilePath, $curPage);
                $rowsExportedCountFilePath = $saveDirPath.'/rows_exported_count.txt';
                $rowsExportedCount = file_exists($rowsExportedCountFilePath) && !empty(file_get_contents($rowsExportedCountFilePath))
                    ? intval(file_get_contents($rowsExportedCountFilePath)) + count($rows)
                    : count($rows) - 1;
                file_put_contents($rowsExportedCountFilePath,  $rowsExportedCount);
                $this->log('total '.$rowsExportedCount.' of '.$totalCount.' was exported', $logFilePath);
                $rows=[];
                $pageIndex = 0;
            }
            $pageIndex++;
        }

        $this->log('FINISH customers export '.$rowsExportedCount.' customers was exported', $logFilePath);

        return $rowsExportedCount;
    }

    /**
     * @param $rows
     * @param $dirPath
     * @param $fileName
     * @return bool
     */
    public function exportCsv($rows, $dirPath, $fileName)
    {
        $this->createDirWritableToAll($dirPath);
        $fh = fopen($dirPath.DIRECTORY_SEPARATOR.$fileName, 'a+');
        foreach ($rows as $row) {
            fputcsv($fh, $row, \Yii::$app->params['csvDelimiter']);
        }
        fclose($fh);

        return true;
    }

    /**
     * @param $customerId
     * @param null $customer
     * @return array
     * @throws ApiException
     */
    public function getCustomerDetails($customerId, $customer=null)
    {
        $result = [];
        /** @var Customer $customer */
        $customer = $customer ?: $this->getApiClient()->getCustomer($customerId);

        /** @var Address $addr */
        if ($addr = $customer->getAddress()) {
            $result['country'] = $addr->getCountry();
            $result['state'] = $addr->getState();
            $result['city'] = $addr->getCity();
            $result['postalCode'] = $addr->getPostalCode();
            $result['addressLines'] = implode(',', $addr->getLines());
            $result['addressCreatedAt'] = $addr->getCreatedAt();
            $result['addressModifiedAt'] = $addr->getModifiedAt();
        }
        else {
            $result['country'] = '';
            $result['state'] = '';
            $result['city'] = '';
            $result['postalCode'] = '';
            $result['addressLines'] = '';
            $result['addressCreatedAt'] = '';
            $result['addressModifiedAt'] = '';
        }


        $result['emails'] = '';
        $result['emailsType'] = '';
        if ($customer->getAddress()) {
            /** @var EmailEntry $curItem */
            foreach($customer->getEmails() as $curItem) {
                $result['emails'][] = $curItem->getValue();
                $result['emailsType'][] = $curItem->getType();
            }
        }

        $result['emails'] = '';
        $result['emailsType'] = '';
        if ($customer->getEmails()) {
            /** @var EmailEntry $curItem */
            foreach($customer->getEmails() as $curItem) {
                $result['emails'][] = $curItem->getValue();
                $result['emailsType'][] = $curItem->getType();
            }
        }
        $result['emails'] = $this->getCsvList($result['emails']);
        $result['emailsType'] = $this->getCsvList($result['emailsType']);

        $result['phones'] = '';
        $result['phonesType'] = '';
        if ($customer->getPhones()) {
            /** @var PhoneEntry $curItem */
            foreach($customer->getPhones() as $curItem) {
                $result['phones'][] = $curItem->getValue();
                $result['phonesType'][] = $curItem->getType();
            }
        }
        $result['phones'] = $this->getCsvList($result['phones']);
        $result['phonesType'] = $this->getCsvList($result['phonesType']);

        $result['websites'] = '';
        $result['websitesTypes'] = '';
        if ($customer->getWebsites()) {
            /** @var WebsiteEntry $curItem */
            foreach($customer->getWebsites() as $curItem) {
                $result['websites'][] = $curItem->getValue();
                $result['websitesTypes'][] = $curItem->getType();
            }
        }
        $result['websites'] = $this->getCsvList($result['websites']);
        $result['websitesTypes'] = $this->getCsvList($result['websitesTypes']);

        $result['socialProfiles'] = '';
        $result['socialProfilesTypes'] = '';
        if ($profiles = $customer->getSocialProfiles()) {
			
            /** @var WebsiteEntry $curItem */
            foreach($profiles as $curItem) {
                $result['socialProfiles'][] = $curItem->getValue();
                $result['socialProfilesTypes'][] = $curItem->getType();
            }
        }
        $result['socialProfiles'] = $this->getCsvList($result['socialProfiles']);
        $result['socialProfilesTypes'] = $this->getCsvList($result['socialProfilesTypes']);

        $result['chats'] = '';
        $result['chatsTypes'] = '';
        if ($customer->getChats()) {
            /** @var ChatEntry $curItem */
            foreach($customer->getChats() as $curItem) {
                $result['chats'][] = $curItem->getValue();
                $result['chatsTypes'][] = $curItem->getType();
            }
        }
        $result['chats'] = $this->getCsvList($result['chats']);
        $result['chatsTypes'] = $this->getCsvList($result['chatsTypes']);

        return $result;
    }

    /**
     * @param $vars
     * @param int $parentId
     * @param $attachmentsDirPath
     * @return array
     */
    public function parseConversationInArray($vars, $parentId=0, $attachmentsDirPath)
    {
        $data = [];
        $data['id'] = isset($vars['id']) ? $vars['id'] : '';
        $data['type'] = isset($vars['type']) ? $vars['type'] : '';
        $data['draft'] = isset($vars['draft']) ? intval($vars['draft']) : '';
        $data['subject'] = isset($vars['subject']) ? $vars['subject'] : '';
        $data['createdAt'] = isset($vars['createdAt']) ? $vars['createdAt'] : '';
        $data['modifiedAt'] = isset($vars['modifiedAt']) ? $vars['modifiedAt'] : '';
        $data['userModifiedAt'] = isset($vars['userModifiedAt']) ? $vars['userModifiedAt'] : '';
        $data['closedAt'] = isset($vars['closedAt']) ? $vars['closedAt'] : '';
        $data['parentId'] = $parentId;
        $data['mailboxId'] = isset($vars['mailbox']['id']) ? $vars['mailbox']['id'] : '';
        $data['mailboxName'] = isset($vars['mailbox']['name']) ? $vars['mailbox']['name'] : '';

        if (isset($vars['threads'])) {
            $data['body'] = '';
        }
        else {
            $data['body'] = str_replace("\n", "\r\n", $vars['body']); // it's required to display content with new lines in cell in Excel
        }

        if (isset($vars['source'])) {
            if (isset($vars['source']->type)) {
                $data['sourceType'] = $vars['source']->type;
                $data['sourceVia'] = $vars['source']->via;
            }
        }
        else {
            $data['sourceType'] = '';
            $data['sourceVia'] = '';
        }

        $data['cc'] = !empty($vars['cc']) ? implode(',', $vars['cc']) : '';
        $data['bcc'] = !empty($vars['bcc']) ? implode(',', $vars['bcc']) : '';
        $data['tags'] = !empty($vars['tags']) ? implode(',', $vars['tags']) : '';
        $data['folderId'] = isset($vars['folderId']) ? $vars['folderId'] : 0;

        $data = $this->addPersonData($vars, $data, 'owner');
        $data = $this->addPersonData($vars, $data, 'customer');
        $data = $this->addPersonData($vars, $data, 'createdBy');
        $data = $this->addPersonData($vars, $data, 'closedBy');

        $data['agentEmail'] = $this->getAgentEmail($vars);

        if (isset($vars['attachments'])) {
            foreach ($vars['attachments'] as $curAttachment) {
                file_put_contents($attachmentsDirPath.'/'.$curAttachment['id'], file_get_contents($curAttachment['url']));
            }
            $data['attachments'] = json_encode($vars['attachments']);
        }
        else {
            $data['attachments'] = '';
        }

        return $data;
    }

    /**
     * @return ApiClient
     * @throws ApiException
     */
    public function getApiClient()
    {
        try {
            $client = ApiClient::getInstance();
            $client->setKey(\Yii::$app->params['helpScoutApiKey']);
        }
        catch (ApiException $e) {
            throw new ApiException($e->getCode().' '.$e->getMessage());
        }

        return $client;
    }

    /**
     * @return int
     * @throws ApiException
     */
    public function exportUsers()
    {
        $resultDirPath = $this->getResultDirPath().'/users';
        $this->createDirWritableToAll($resultDirPath);
        $logFilePath = $resultDirPath.'/log.txt';
        $this->log('START export users', $logFilePath);

        try {
            $collection = $this->getApiClient()->getUsers();
        }
        catch(ApiException $ex) {
            throw new ApiException($ex->getMessage(), $ex->getCode());
        }

        $pagesCount = $collection->getCount();
        $hasHeaders = false;
        $rowsCount = 0;

        for ($curPage = 1; $curPage <= $pagesCount; $curPage++) {
            $users = $this->getApiClient()->getUsers($curPage);
            /** @var User $user */
            foreach ($users->getItems() as $user) {
                $rows[] = [
                    'id' => $user->getId(),
                    'firstName' => $user->getFirstName(),
                    'lastName' => $user->getLastName(),
                    'email' => $user->getEmail(),
                    'role' => $user->getRole(),
                    'timezone' => $user->getTimezone(),
                    'photoUrl' => $user->getPhotoUrl(),
                    'createdAt' => $user->getCreatedAt(),
                    'modifiedAt' => $user->getModifiedAt()
                ];
                $rowsCount++;
            }
            if (!$hasHeaders) {
                $firstRow = array_pop($rows);
                if (empty($firstRow)) {
                    continue;
                }
                $rows[] = array_keys($firstRow);
                $rows[] = $firstRow;
                $hasHeaders = true;
            }
            $this->exportCsv($rows, $resultDirPath, 'helpscout_users.csv');
            $rows = [];
        }

        $this->log('FINISH export users. '.$rowsCount.' users was exported', $logFilePath);

        return $rowsCount;
    }

    /**
     * @param $logMsg
     * @param $saveToFile
     * @return bool
     */
    protected function log($logMsg, $saveToFile)
    {
        $logMsg = date('Y-m-d H:i:s')."\t".$logMsg;
        Console::output($logMsg);

        return file_put_contents($saveToFile, $logMsg."\r\n", FILE_APPEND);
    }

    /**
     * Agent email is support employee's email
     * @param array $data
     * @return string
     */
    protected function getAgentEmail($data)
    {
        $agentEmailDomain = \Yii::$app->params['agentEmailDomain'];
        if (!empty($data['closedByEmail'])) {
            list($mailBox, $mailDomain) = explode('@', $data['closedByEmail']);
            if ($mailDomain == $agentEmailDomain) {
                return $data['closedByEmail'];
            }
        }
        if (!empty($data['assignedToEmail'])) {
            list($mailBox, $mailDomain) = explode('@', $data['assignedToEmail']);
            if ($mailDomain == $agentEmailDomain) {
                return $data['assignedToEmail'];
            }
        }
        if (!empty($data['createdByEmail'])) {
            list($mailBox, $mailDomain) = explode('@', $data['createdByEmail']);
            if ($mailDomain == $agentEmailDomain) {
                return $data['createdByEmail'];
            }
        }

        return '';
    }

    /**
     * @param array $from
     * @param array $to
     * @param $personType
     * @return mixed
     */
    protected function addPersonData($from, $to, $personType)
    {
        if (empty($from[$personType])) {
            $to[$personType.'Id'] = '';
            $to[$personType.'FirstName'] = '';
            $to[$personType.'LastName'] = '';
            $to[$personType.'Email'] = '';
            $to[$personType.'Phone'] = '';
            $to[$personType.'Type'] = '';
        }
        else {
            $to[$personType.'Id'] = $from[$personType]['id'];
            $to[$personType.'FirstName'] = $from[$personType]['firstName'];
            $to[$personType.'LastName'] = $from[$personType]['lastName'];
            $to[$personType.'Email'] = $from[$personType]['email'];
            $to[$personType.'Phone'] = $from[$personType]['phone'];
            $to[$personType.'Type'] = $from[$personType]['type'];
        }

        return $to;
    }

    /**
     * @param $array
     * @return string
     */
    protected function getCsvList($array) {
        return $array ? implode(',', $array) : '';
    }

    /**
     * @return string
     */
    protected function getResultDirPath()
    {
        return realpath(__DIR__.'/..').'/data/helpscout_exported/'.substr(\Yii::$app->params['helpScoutApiKey'], 0, 5).'/';
    }

    /**
     * @param $dirPath
     * @return bool
     */
    protected function createDirWritableToAll($dirPath)
    {
        if (!file_exists($dirPath)) {
            return mkdir($dirPath, 0777, true);
        }

        return true;
    }
}