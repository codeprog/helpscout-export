HelpScoutExport

Application that returns the result data set page by page. Allows to automatically resume export from the last exported page.
Exported data is saved to directory: data/helpscout_exported/[first 5 symbols of your HelpScout API key]/[entity name]
------------------------------------------------
Usage:
Set parameters in config/params.php file.

1) # php yii help-scout-export/export-users
To export all users.

2) # php yii help-scout-export/export-messages mailboxId[required] modifiedSince[required] status[can be "all" OR "active" OR "pending"] tag[any of tags in CSV format]

To export customers from mailbox set mailboxId. For example:
Export all messages from mailbox with id 123456, modified since 2017-05-20T10:00:00Z
# php yii help-scout-export/export-messages 123456 2017-05-20T10:00:00Z
To export messages from all mailboxes, modified since 2017-05-20T10:00:00Z, with "all" statuses and that have been tagged "todo" or "feature-request":
# php yii help-scout-export/export-messages all 2017-05-20T10:00:00Z all todo,feature-request

3) # php yii help-scout-export/export-customers pagesCountFrom pagesCountTo isResumeFromLastExportedPage
Exporting customers is quite slow operation, because to get the data of each client we need to run 2 different API calls.
For example:
# php yii help-scout-export/export-customers 1 all 1
Will export all customers from all pages with resume from last exported page.

# php yii help-scout-export/export-customers 3 10 0
Will export all customers from page 3 to page 10 without resume from last exported page.

